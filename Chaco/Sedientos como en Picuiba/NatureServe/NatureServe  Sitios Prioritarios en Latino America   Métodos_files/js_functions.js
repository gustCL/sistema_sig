function clearField(formName, fieldName)
{
	var tempField = document.forms[formName].elements[fieldName];
	if(tempField.value == tempField.defaultValue)
	{
		tempField.value = '';
	}
}

function restoreField(formName, fieldName)
{
	var tempField = document.forms[formName].elements[fieldName];
	if(tempField.value == '')
	{
		tempField.value = tempField.defaultValue;
	}
}

function WM_preloadImages() 
{
	/*
	WM_preloadImages(): Loads images into the browser's cache for later use.
	Source: Webmonkey Code Library -- (http://www.hotwired.com/webmonkey/javascript/code_library/)
	Author: Nadav Savio - nadav@wired.com
	Usage: WM_preloadImages('image 1 URL', 'image 2 URL', 'image 3 URL', ...);
	*/
	// Don't bother if there's no document.images
	if (document.images) 
	{
		if (typeof(document.WM) == 'undefined')
		{
			document.WM = new Object();
		}
		document.WM.loadedImages = new Array();
		// Loop through all the arguments.
		var argLength = WM_preloadImages.arguments.length;
		for(arg=0;arg<argLength;arg++)
		{
			// For each arg, create a new image.
			document.WM.loadedImages[arg] = new Image();
			// Then set the source of that image to the current argument.
			document.WM.loadedImages[arg].src = WM_preloadImages.arguments[arg];
		}
	}
}

//----------------------------------------------

function WM_imageSwap(daImage, daSrc)
{
	/*
	WM_imageSwap(): Changes the source of an image.
	Source: Webmonkey Code Library -- (http://www.hotwired.com/webmonkey/javascript/code_library/)
	Authors: Shvatz (shvatz@wired.com), Ken Sundermeyer (ksundermeyer@macromedia.com)
	Usage: WM_imageSwap(originalImage, 'newSourceUrl');
	Requires: WM_preloadImages() (optional, but recommended)
	*/
	var objStr,obj;
	// Check to make sure that images are supported in the DOM.
	if(document.images)
	{
		// Check to see whether you are using a name, number, or object
		if (typeof(daImage) == 'string')
		{
			// This whole objStr nonesense is here solely to gain compatability with ie3 for the mac.
			objStr = 'document.' + daImage;
			obj = eval(objStr);
			obj.src = daSrc;
		}
		else if ((typeof(daImage) == 'object') && daImage && daImage.src)
		{
			daImage.src = daSrc;
		}
	}
}

  (function() {
    var cx = '008517670119938511799:tvg5cbygjhc';
    var gcse = document.createElement('script'); gcse.type = 'text/javascript'; gcse.async = true;
    gcse.src = (document.location.protocol == 'https:' ? 'https:' : 'http:') +
        '//www.google.com/cse/cse.js?cx=' + cx;
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(gcse, s);
  })();

