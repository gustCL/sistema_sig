 /***********************************************
*	(c) Ger Versluis 2000 version 8.23 2 October 2002        *
*	You may use this script on non commercial sites.	          *
*	For info write to menus@burmees.nl		          *
*	You may remove all comments for faster loading	          *		
************************************************/
	var NoOffFirstLineMenus=8;			// Number of main menu  items
						// Colorvariables:
						// Color variables take HTML predefined color names or "#rrggbb" strings
						//For transparency make colors and border color ""
	var LowBgColor="#FEF9AA";			// Background color when mouse is not over
	var HighBgColor="white";			// Background color when mouse is over
	var FontLowColor="black";			// Font color when mouse is not over
	var FontHighColor="black";			// Font color when mouse is over
	var BorderColor="white";			// Border color
	var BorderWidthMain=0;			// Border width main items
	var BorderWidthSub=1;			// Border width sub items
 	var BorderBtwnMain=0;			// Border between elements main items 1 or 0
	var BorderBtwnSub=1;			// Border between elements sub items 1 or 0
	var FontFamily="verdana,arial,times roman";	// Font family menu items
	var FontSize=8;				// Font size menu items
	var FontBold=0;				// Bold menu items 1 or 0
	var FontItalic=0;				// Italic menu items 1 or 0
	var MenuTextCentered="left";		// Item text position left, center or right
	var MenuCentered="left";			// Menu horizontal position can be: left, center, right, justify,
						//  leftjustify, centerjustify or rightjustify. PartOfWindow determines part of window to use
	var MenuVerticalCentered="top";		// Menu vertical position top, middle,bottom or static
	var ChildOverlap=.2;				// horizontal overlap child/ parent
	var ChildVerticalOverlap=.2;			// vertical overlap child/ parent
	var StartTop=0;				// Menu offset x coordinate
	var StartLeft=4;				// Menu offset y coordinate
	var VerCorrect=0;				// Multiple frames y correction
	var HorCorrect=2;				// Multiple frames x correction
	var LeftPaddng=6;				// Left padding
	var TopPaddng=6;				// Top padding
	var FirstLineHorizontal=1;			// First level items layout horizontal 1 or 0
	var MenuFramesVertical=1;			// Frames in cols or rows 1 or 0
	var DissapearDelay=1000;			// delay before menu folds in
	var UnfoldDelay=100;			// delay before sub unfolds	
	var TakeOverBgColor=1;			// Menu frame takes over background color subitem frame
	var FirstLineFrame="self";			// Frame where first level appears
	var SecLineFrame="space";			// Frame where sub levels appear
	var DocTargetFrame="space";			// Frame where target documents appear
	var TargetLoc="MenuPossub";				// span id for relative positioning
	var MenuWrap=1;				// enables/ disables menu wrap 1 or 0
	var RightToLeft=0;				// enables/ disables right to left unfold 1 or 0
	var BottomUp=0;				// enables/ disables Bottom up unfold 1 or 0
	var UnfoldsOnClick=0;			// Level 1 unfolds onclick/ onmouseover
	var BaseHref="";				// BaseHref lets you specify the root directory for relative links. 
						// The script precedes your relative links with BaseHref
						// For instance: 
						// when your BaseHref= "http://www.MyDomain/" and a link in the menu is "subdir/MyFile.htm",
						// the script renders to: "http://www.MyDomain/subdir/MyFile.htm"
						// Can also be used when you use images in the textfields of the menu
						// "MenuX=new Array("<img src=\""+BaseHref+"MyImage\">"
						// For testing on your harddisk use syntax like: BaseHref="file:///C|/MyFiles/Homepage/"

	var Arrws=[BaseHref+"",5,10,BaseHref+"",10,5,BaseHref+"",5,10,BaseHref+"",10,5];

						// Arrow source, width and height.
						// If arrow images are not needed keep source ""

	var MenuUsesFrames=0;			// MenuUsesFrames is only 0 when Main menu, submenus,
						// document targets and script are in the same frame.
						// In all other cases it must be 1

	var RememberStatus=0;			// RememberStatus: When set to 1, menu unfolds to the presetted menu item. 
						// When set to 2 only the relevant main item stays highligthed
						// The preset is done by setting a variable in the head section of the target document.
						// <head>
						//	<script type="text/javascript">var SetMenu="2_2_1";</script>
						// </head>
						// 2_2_1 represents the menu item Menu2_2_1=new Array(.......
	var PartOfWindow=.8;			// PartOfWindow: When MenuCentered is justify, sets part of window width to stretch to

						// Below some pretty useless effects, since only IE6+ supports them
						// I provided 3 effects: MenuSlide, MenuShadow and MenuOpacity
						// If you don't need MenuSlide just leave in the line var MenuSlide="";
						// delete the other MenuSlide statements
						// In general leave the MenuSlide you need in and delete the others.
						// Above is also valid for MenuShadow and MenuOpacity
						// You can also use other effects by specifying another filter for MenuShadow and MenuOpacity.
						// You can add more filters by concanating the strings
	var MenuSlide="";
	var MenuSlide="progid:DXImageTransform.Microsoft.RevealTrans(duration=.5, transition=19)";
	var MenuSlide="progid:DXImageTransform.Microsoft.GradientWipe(duration=.5, wipeStyle=1)";

	var MenuShadow="";
	/*var MenuShadow="progid:DXImageTransform.Microsoft.DropShadow(color=#888888, offX=2, offY=2, positive=1)";
	var MenuShadow="progid:DXImageTransform.Microsoft.Shadow(color=#888888, direction=135, strength=3)";
*/
	var MenuOpacity="";
	/*var MenuOpacity="progid:DXImageTransform.Microsoft.Alpha(opacity=75)";
*/
	function BeforeStart(){return}
	function AfterBuild(){return}
	function BeforeFirstOpen(){return}
	function AfterCloseAll(){return}

// Menu tree:
// MenuX=new Array("ItemText","Link","background image",number of sub elements,height,width,"bgcolor","bghighcolor",
//	"fontcolor","fonthighcolor","bordercolor","fontfamily",fontsize,fontbold,fontitalic,"textalign","statustext");
// Color and font variables defined in the menu tree take precedence over the global variables
// Fontsize, fontbold and fontitalic are ignored when set to -1.
// For rollover images ItemText format is:  "rollover?"+BaseHref+"Image1.jpg?"+BaseHref+"Image2.jpg" 

Menu1=new Array("rollover?"+BaseHref+"images/home1.gif?"+BaseHref+"images/home2.gif","/index.jsp","",0,19,64,"","","","","","",-1,-1,-1,"","");
	//Menu1_1=new Array("Home","index.jsp","",0,20,100,"black","white","yellow","#004678","yellow","",-1,-1,-1,"","");
	
Menu2=new Array("rollover?"+BaseHref+"images/aboutus1.gif?"+BaseHref+"images/aboutus2.gif","/aboutUs/index.jsp","",7,19,75,"","","","","","",-1,-1,-1,"","");
	Menu2_1=new Array("Who We Are","/aboutUs/index.jsp","",0,20,100,"#004678","white","white","#004678","#004678","",-1,-1,-1,"","");	
	Menu2_2=new Array("Partners","/aboutUs/partners.jsp","",0,20,100,"#004678","white","white","#004678","#004678","",-1,-1,-1,"","");
	Menu2_3=new Array("Projects","/projects/united_states.jsp","",0,20,100,"#004678","white","white","#004678","#004678","",-1,-1,-1,"","");
	<!--Menu2_4=new Array("Staff Expertise","../aboutUs/staffexpertise.jsp","",0,20,100,"#004678","white","white","#004678","#004678","",-1,-1,-1,"","");-->
	Menu2_4=new Array("Staff List","http://whiteoak.natureserve.org/hsds","",0,20,100,"#004678","white","white","#004678","#004678","",-1,-1,-1,"","");
	Menu2_5=new Array("Newsroom","/aboutUs/newsroom.jsp","",0,20,100,"#004678","white","white","#004678","#004678","",-1,-1,-1,"","");
	Menu2_6=new Array("Jobs","/aboutUs/jobs.jsp","",0,20,100,"#004678","white","white","#004678","#004678","",-1,-1,-1,"","");
	Menu2_7=new Array("Support Us","/supportUs/index.jsp","",0,20,150,"#004678","white","white","#004678","#004678","",-1,-1,-1,"","");

Menu3=new Array("rollover?"+BaseHref+"images/projects1.gif?"+BaseHref+"images/projects2.gif","/projects/united_states.jsp","",3,19,75,"","","","","","",-1,-1,-1,"","");
	Menu3_1=new Array("United States","/projects/united_states.jsp","",0,20,160,"#004678","white","white","#004678","#004678","",-1,-1,-1,"","");	
	Menu3_2=new Array("Canada","/projects/canada.jsp","",0,20,100,"#004678","white","white","#004678","#004678","",-1,-1,-1,"","");
	Menu3_3=new Array("Latin America","/projects/latin_america.jsp","",0,20,100,"#004678","white","white","#004678","#004678","",-1,-1,-1,"","");
	
Menu4=new Array("rollover?"+BaseHref+"images/visitprograms1.gif?"+BaseHref+"images/visitprograms2.gif","/visitLocal/index.jsp","",6,19,141,"","","","","","",-1,-1,-1,"","");		
//Menu3=new Array("Example 3","file3.htm","",0,20,100,"","","","","","",-1,-1,-1,"","");
	//Menu3_1=new Array("Network Members","../visitLocal/netMemb.htm","",0,20,160,"#004678","white","white","#004678","#004678","",-1,-1,-1,"","");
	Menu4_1=new Array("Local Program Websites","/visitLocal/index.jsp","",0,20,190,"#004678","white","white","#004678","#004678","",-1,-1,-1,"","");
	Menu4_2=new Array("NatureServe Canada","javascript:window.open('http://www.natureserve-canada.ca', '_blank');","",0,20,190,"#004678","white","white","#004678","#004678","",-1,-1,-1,"","");
	Menu4_3=new Array("Conservation Conference 2007","/visitLocal/cons_conferencepresentations.jsp","",0,20,190,"#004678","white","white","#004678","#004678","",-1,-1,-1,"","");
	Menu4_4=new Array("Conferences & Training","/visitLocal/conferencesTraining.jsp","",0,20,190,"#004678","white","white","#004678","#004678","",-1,-1,-1,"","");
	Menu4_5=new Array("Events Calendars","/visitLocal/calendar.jsp","",0,20,190,"#004678","white","white","#004678","#004678","",-1,-1,-1,"","");
	Menu4_6=new Array("Network Staff Directory","http://whiteoak.natureserve.org/hsds","",0,20,190,"#004678","white","white","#004678","#004678","",-1,-1,-1,"","");
	
Menu5=new Array("rollover?"+BaseHref+"images/getdata1.gif?"+BaseHref+"images/getdata2.gif","/getData/index.jsp","",9,19,76,"","","","","","",-1,-1,-1,"","");
	Menu5_1=new Array("NatureServe Explorer","javascript:window.open('http://www.natureserve.org/explorer', '_blank');","",0,20,190,"#004678","white","white","#004678","#004678","",-1,-1,-1,"","");	
	Menu5_2=new Array("InfoNatura","javascript:window.open('http://www.natureserve.org/infonatura', '_blank');","",0,20,190,"#004678","white","white","#004678","#004678","",-1,-1,-1,"","");
	Menu5_3=new Array("Global Amphibian Assessment","javascript:window.open('http://www.globalamphibians.org', '_blank');","",0,20,190,"#004678","white","white","#004678","#004678","",-1,-1,-1,"","");
	Menu5_4=new Array("Local Program Data","/getData/programData.jsp","",0,20,190,"#004678","white","white","#004678","#004678","",-1,-1,-1,"","");
	Menu5_5=new Array("Animal Data for Download","/getData/animalData.jsp","",0,20,190,"#004678","white","white","#004678","#004678","",-1,-1,-1,"","");
	Menu5_6=new Array("Ecology Data","/getData/ecologyData.jsp","",0,20,190,"#004678","white","white","#004678","#004678","",-1,-1,-1,"","");
	Menu5_7=new Array("Plant Data for Download","/getData/plantData.jsp","",0,20,190,"#004678","white","white","#004678","#004678","",-1,-1,-1,"","");
	<!--Menu4_6=new Array("Bird Distribution Maps","../getData/birdMaps.jsp","",0,20,165,"#004678","white","white","#004678","#004678","",-1,-1,-1,"","");-->
	Menu5_8=new Array("Custom Data Services","/getData/customData.jsp","",0,20,190,"#004678","white","white","#004678","#004678","",-1,-1,-1,"","");	
	Menu5_9=new Array("Data Links","/getData/dataResources.jsp","",0,20,190,"#004678","white","white","#004678","#004678","",-1,-1,-1,"","");
	
	
Menu6=new Array("rollover?"+BaseHref+"images/prodservices1.gif?"+BaseHref+"images/prodservices2.gif","/prodServices/index.jsp","",8,19,138,"","","","","","",-1,-1,-1,"","");
	Menu6_1=new Array("NatureServe Web Services","javascript:window.open('http://services.natureserve.org/', '_blank');","",0,20,190,"#004678","white","white","#004678","#004678","",-1,-1,-1,"","");	
	Menu6_2=new Array("NatureServe Vista Software","/prodServices/vista/overview.jsp","",0,20,190,"#004678","white","white","#004678","#004678","",-1,-1,-1,"","");
	Menu6_3=new Array("Biotics 4 Software","/prodServices/biotics.jsp","",0,20,190,"#004678","white","white","#004678","#004678","",-1,-1,-1,"","");
	Menu6_4=new Array("Ecosystem Mapping","/prodServices/ecomapping.jsp","",0,20,190,"#004678","white","white","#004678","#004678","",-1,-1,-1,"","");
	Menu6_5=new Array("Predictive Distribution Modeling","/prodServices/predictiveDistModeling.jsp","",0,20,190,"#004678","white","white","#004678","#004678","",-1,-1,-1,"","");
	Menu6_6=new Array("Expert Consultation","/prodServices/expertconsult.jsp","",0,20,190,"#004678","white","white","#004678","#004678","",-1,-1,-1,"","");
	Menu6_7=new Array("Information Technology & Tools","/prodServices/infoTechnology.jsp","",0,20,190,"#004678","white","white","#004678","#004678","",-1,-1,-1,"","");
	Menu6_8=new Array("Standards & Methods","/prodServices/standardsMethods.jsp","",0,20,190,"#004678","white","white","#004678","#004678","",-1,-1,-1,"","");
	
		
Menu7=new Array("rollover?"+BaseHref+"images/publications1.gif?"+BaseHref+"images/publications2.gif","/publications/index.jsp","",3,19,92,"","","","","","",-1,-1,-1,"","");
	Menu7_1=new Array("NatureServe Publications","/publications/library.jsp#nspubs","",0,20,190,"#004678","white","white","#004678","#004678","",-1,-1,-1,"","");	
	Menu7_2=new Array("Member Program Publications","/publications/library.jsp#memberpubs","",0,20,190,"#004678","white","white","#004678","#004678","",-1,-1,-1,"","");
	Menu7_3=new Array("NatureServe Technical Reports","/publications/library.jsp#techrpts","",0,20,190,"#004678","white","white","#004678","#004678","",-1,-1,-1,"","");
/*	Menu6_4=new Array("Staff Scientific Publications","../publications/library.jsp#scientificpubs","",0,20,190,"#004678","white","white","#004678","#004678","",-1,-1,-1,"","");
*/

Menu8=new Array("rollover?"+BaseHref+"images/supportus1.gif?"+BaseHref+"images/supportus2.gif","/supportUs/index.jsp","",7,19,154,"","","","","","",-1,-1,-1,"","");
	Menu8_1=new Array("Corporate Funder","/supportUs/corporate.jsp","",0,20,155,"#004678","white","white","#004678","#004678","",-1,-1,-1,"","");	
	<!--Menu7_2=new Array("Ecosystems","../consIssues/ecosystems.jsp","",0,20,130,"#004678","white","white","#004678","#004678","",-1,-1,-1,"","");-->
	Menu8_2=new Array("Private Foundation Funder","/supportUs/private_fdn_funder.jsp","",0,20,155,"#004678","white","white","#004678","#004678","",-1,-1,-1,"","");
	Menu8_3=new Array("Individual Donor","/supportUs/indiv_donor.jsp","",0,20,155,"#004678","white","white","#004678","#004678","",-1,-1,-1,"","");
	Menu8_4=new Array("Whom Do I Contact?","/supportUs/contacts.jsp","",0,20,155,"#004678","white","white","#004678","#004678","",-1,-1,-1,"","");
	Menu8_5=new Array("Funding Opportunities","/supportUs/funding_opps.jsp","",0,20,155,"#004678","white","white","#004678","#004678","",-1,-1,-1,"","");
	Menu8_6=new Array("Donor Bill of Rights","/supportUs/donor_rights.jsp","",0,20,155,"#004678","white","white","#004678","#004678","",-1,-1,-1,"","");
	Menu8_7=new Array("Financial Disclosure","/supportUs/fin_disclosure.jsp","",0,20,155,"#004678","white","white","#004678","#004678","",-1,-1,-1,"","");
