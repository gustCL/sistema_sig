﻿<%@ Page Title="Página principal" Language="vb" MasterPageFile="~/Site.Master" AutoEventWireup="false"
    CodeBehind="Default.aspx.vb" Inherits="ProyectoChaco._Default" %>

<%@ Register assembly="AspMapNET" namespace="AspMap.Web" tagprefix="aspmap" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <style type="text/css">
    .style1
    {
        width: 100%;
    }
    .style2
    {
        width: 614px;
    }
</style>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <h2>
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
    </h2>
    <p>
        <aspmap:MapToolButton ID="MapToolButton3" runat="server" 
            ImageUrl="~/tools/pan.gif" MapTool="Pan" />
&nbsp;<aspmap:MapToolButton ID="MapToolButton2" runat="server" 
            ImageUrl="~/tools/zoomout.gif" MapTool="ZoomOut" />
&nbsp;<aspmap:MapToolButton ID="MapToolButton1" runat="server" 
            ImageUrl="~/tools/zoomin.gif" SelectedImageUrl="" />
    </p>
    <table class="style1">
        <tr>
            <td>
                &nbsp; </td>
            <td class="style2">
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>

                <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                    <ContentTemplate >
                        &nbsp;&nbsp;&nbsp;&nbsp;
                        <asp:Label ID="Label1" runat="server" Font-Bold="True" Font-Italic="True" 
                            Text="CAPAS"></asp:Label>
                        <br />
                        <asp:CheckBoxList ID="layerlist" runat="server" AutoPostBack="True">
                        </asp:CheckBoxList>    
                        <br />
                        &nbsp;
                        <asp:CheckBox ID="CheckBox1" runat="server" Text="google Map" 
                            AutoPostBack="True" Checked="True" Visible="False" />
                    </ContentTemplate>
                </asp:UpdatePanel>

            </td>
            <td class="style2">
                <aspmap:Map ID="Map1" runat="server" Height="430px" Width="663px" />
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td class="style2">
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
</table>
    <p>
        <asp:DropDownList ID="DropDownList1" runat="server">
        </asp:DropDownList>
&nbsp;<asp:Button ID="Button1" runat="server" Text="Button" />
    </p>
    <p>
        También puede encontrar <a href="http://go.microsoft.com/fwlink/?LinkID=152368"
            title="Documentación de ASP.NET en MSDN">documentación sobre ASP.NET en MSDN</a>.
    </p>
</asp:Content>
