﻿Imports AspMap
Imports AspMap.Web

Imports System.Drawing

Imports System
Imports System.Web.UI
Imports AspMap.Web.Extensions

Imports ServerControlConverter
Imports AspMap.CoordSystem

Imports Microsoft.VisualBasic

Imports System.Collections
Imports System.ComponentModel
Imports System.Data

Imports System.Web
Imports System.Web.SessionState

Imports System.Web.UI.WebControls
Imports System.Web.UI.HtmlControls

Public Class _Default
    Inherits System.Web.UI.Page

#Region "adicionando mapas"
    Private Sub addMapas()
        Dim layer As AspMap.Layer
        'Dim layer1 As Layer

        Dim MapDir As String = MapPath("shape/")


        layer = Map1.AddLayer(MapDir & "Departamentos.shp")
        layer.Description = "Departamentos"

        layer = Map1.AddLayer(MapDir & "CentrosPoblados.shp")
        'layer.Visible = True
        layer.Description = "Centros Poblados"

    End Sub
#End Region

#Region "propiedades de los shapes"
    Private Sub PropiedadesShapes()
        Dim layer As AspMap.Layer
        'Dim layer1 As Layer

        Dim MapDir As String = MapPath("shape/")

        'propiedades de los Modulos
        layer = Map1("Departamentos")
        layer.LabelField = "Nombre"
        layer.Symbol.FillColor = Color.Tomato
        layer.ShowLabels = True
        layer.LabelFont.Name = "verdana"
        layer.LabelFont.Color = Color.Black
        layer.LabelFont.Size = 16
        layer.LabelFont.Bold = True
        layer.LabelStyle = LabelStyle.PolygonCenter
        layer.Symbol.Size = 2

        '-------------PROPIEDADES TIPO LINEAS-------------------------------'

        '' adiciaonando vias Vehiculares
        'layer = Map1("VEHICULAR")
        'layer.Symbol.LineColor = Drawing.Color.Yellow
        'layer.ShowLabels = False
        'layer.LabelFont.Size = 2
        'layer.Symbol.Size = 4


        ''  via peatonal
        'layer = Map1("PEATONAL")
        'layer.Symbol.LineColor = Drawing.Color.Brown
        'layer.ShowLabels = False
        'layer.LabelFont.Size = 2
        'layer.Symbol.Size = 1


        '' ADICIONANDO PUNTOS
        ''  Centros Poblados-------------------------
        layer = Map1("CentrosPoblados")
        layer.Symbol.PointStyle = PointStyle.Bitmap
        layer.Symbol.Bitmap = MapPath("Simbolos/school.bmp")
        layer.Symbol.TransparentColor = Color.White
        layer.Symbol.Size = 17
        'layer.Symbol.FillColor = Drawing.Color.Red
        layer.LabelField = "Nombre"
        layer.ShowLabels = False
        layer.LabelFont.Name = "arial"
        layer.LabelFont.Bold = True
        layer.LabelFont.Size = 14
        layer.LabelFont.Outline = True
        layer.LabelFont.OutlineColor = Drawing.Color.Yellow
    End Sub
#End Region

#Region "Mostrando Lista de Capas"
    Private Sub MostrarListaCapas()
        If IsPostBack Then
            Return
        End If

        For Each layer As AspMap.Layer In Map1
            Dim item As ListItem = New ListItem(layer.Description, layer.Name)
            item.Selected = layer.Visible
            layerlist.Items.Add(item)
        Next layer
    End Sub

    Public Sub actualizarVistaCapas()
        If (Not IsPostBack) Then
            Return
        End If

        For Each item As ListItem In layerlist.Items
            Map1(item.Value).Visible = item.Selected
        Next item
    End Sub
#End Region

#Region "cargar datos para seleccionar"
    Public Sub CargarDepartamento()
        If (Not IsPostBack) Then
            DropDownList1.DataSource = Map1("Departamentos").Recordset
            DropDownList1.DataTextField = "Nombre"
            DropDownList1.DataValueField = "Nombre"
            DropDownList1.DataBind()
        End If
    End Sub
#End Region
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        addMapas()
        MostrarListaCapas()
        PropiedadesShapes()
        CargarDepartamento()
    End Sub

    Private Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        actualizarVistaCapas()
    End Sub

    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As EventArgs) Handles Button1.Click

        Dim state As Recordset
        state = Map1("Departamentos").SearchExpression("Nombre=""" & DropDownList1.SelectedValue & """")
        If Not state.EOF Then

            Map1.Extent = state.Extent


            ' select the state shape with a red outline
            Map1.MapShapes.Clear()
            Dim shape As MapShape = Map1.MapShapes.Add(state.Shape)
            Dim s As Symbol = New Symbol()
            s.FillStyle = AspMap.FillStyle.Solid
            s.LineColor = Color.Aqua
            s.Size = 3
            shape.Symbol = s

        End If
    End Sub
End Class