  Private Sub AddModuloTooltips()
        Map1.TooltipStyle.BackColor = Color.LightYellow
        Map1.TooltipStyle.Width = System.Web.UI.WebControls.Unit.Pixel(220)

        Dim records As AspMap.Recordset = Map1("Modulos").SearchShape(Map1.Extent, AspMap.SearchMethod.Inside)

        If Not records.EOF Then

            Map1.Extent = records.Extent

            'Do While Not records.EOF
            Dim tooltip As String = "<b> " & records("Modulo") & "</b>"

            'Map1.MapShapes.Clear()

            'arreglar
            ' Dim shape As MapShape = Map1.MapShapes.Add(records.Shape)
            'Dim s As Symbol = New Symbol()
            ' s.FillStyle = AspMap.FillStyle.Invisible
            '  s.LineColor = Color.Aqua
            '   s.Size = 3
            '  shape.Symbol = s
            '    Map1.MapShapes.Add(records.Shape)



            Map1.Hotspots.AddInfo(records.RecordExtent.Center, 9, tooltip, records("ID").ToString())
            records.MoveNext()
            ' Loop
        End If

    End Sub