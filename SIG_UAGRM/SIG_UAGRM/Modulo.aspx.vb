﻿Public Class Modulo
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim txtBusqueda As String = ""


        If Me.PreviousPage IsNot Nothing Then ' si viene de otra pagina que no sea la actual
            txtBusqueda = DirectCast(Me.PreviousPage.Master.FindControl("tBusqueda"), TextBox).Text  'asignando el contenido que hay en text de otra pag
        Else
            txtBusqueda = DirectCast(Me.Master.FindControl("tBusqueda"), TextBox).Text ''asignando el contenido que hay en text de la pag actual
        End If

        ObjectDataSource1.SelectParameters.Clear()  'borramos todos los parametros

        If String.IsNullOrEmpty(txtBusqueda) Then   ' si el valor del texto esta vacio
            ObjectDataSource1.SelectMethod = "GetData"
            LBusqueda.Text = "Resultado de la busqueda de Modulo sin filtro"
        Else
            ObjectDataSource1.SelectMethod = "GetDataByFiltroModulo"
            ObjectDataSource1.SelectParameters.Add("Modulo", txtBusqueda) ' ("el nombre del filtro",textbox)
            LBusqueda.Text = "Resultado de la busqueda de Nudo con filtro""-" + txtBusqueda + "-"""
        End If
    End Sub

End Class