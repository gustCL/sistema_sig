﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Principal.Master" CodeBehind="index.aspx.vb" Inherits="SIG_UAGRM.index" %>
<%@ Register assembly="AspMapNET" namespace="AspMap.Web" tagprefix="aspmap" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <p style="font-size: small">
    <br />
    <br />
    <br />
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
        <br />
    <br />
    <table style="width: 100%">
        <tr>
            <td colspan="3">
                <aspmap:MapToolButton ID="MapToolButton1" runat="server" 
                    ImageUrl="~/tools/zoomin.gif" Map="Map1" />
&nbsp;<aspmap:MapToolButton ID="MapToolButton2" runat="server" ImageUrl="~/tools/zoomout.gif" 
                    Map="Map1" MapTool="ZoomOut" />
&nbsp;<aspmap:MapToolButton ID="MapToolButton3" runat="server" ImageUrl="~/Simbolos/manito icono.png" 
                    Map="Map1" MapTool="Pan" />
&nbsp;
                <aspmap:MapToolButton ID="MapToolButton5" runat="server" 
                    ImageUrl="~/tools/distance.gif" Map="Map1" MapTool="Distance" />
&nbsp;<aspmap:MapToolButton ID="MapToolButton6" runat="server" 
                    ImageUrl="~/tools/infowindow.gif" Map="Map1" MapTool="InfoWindow" />
&nbsp;

                     <button onclick="printMap()">PrintMap</button>
                            &nbsp;
                       <button onclick="centerAt()">Center at</button> &nbsp;&nbsp;&nbsp; latitud:&nbsp;&nbsp;
                       <input id="lat" size="5" type="text" value="-17.775" style="width: 88px"/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
		        	     longitud:&nbsp;&nbsp; <input id="long" size="5" type="text" value="-63.195" 
                         style="width: 80px"/>
                        &nbsp;
            </td>
        </tr>
        <tr>
            <td style="width: 157px">
               <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                    <ContentTemplate >
                        &nbsp;&nbsp;&nbsp;&nbsp;
                        <asp:Label ID="Label1" runat="server" Font-Bold="True" Font-Italic="True" 
                            Text="CAPAS"></asp:Label>
                        <br />
                        <asp:CheckBoxList ID="layerlist" runat="server" AutoPostBack="True">
                        </asp:CheckBoxList>    
                        <br />
                        &nbsp;
                        <asp:CheckBox ID="CheckBox1" runat="server" Text="google Map" 
                            AutoPostBack="True" Checked="True" />
                    </ContentTemplate>
                </asp:UpdatePanel>
                
            </td>
            <td style="width: 472px">
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                        <aspmap:Map ID="Map1" runat="server" Width="690px" BorderStyle="Solid" 
                            Height="428px" />
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
            <td>
                <table style="width: 92%; height: 165px">
                    <tr>
                        <td>
                            <aspmap:ZoomBar ID="ZoomBar1" runat="server" Map="Map1" 
                                style="top: 0px; left: 0px; height: 148px" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;</td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td style="width: 157px">
                &nbsp;</td>
            <td style="width: 472px">
               <span id="latlong" ></span> &nbsp;
             </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td colspan="3">
                <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                <ContentTemplate>

                    &nbsp;
                    <asp:DropDownList ID="DropDownList2" runat="server" >
                    </asp:DropDownList>
                    &nbsp;<asp:Button ID="Button1" runat="server" Text="Ir a Modulo" 
                        style="height: 26px" />
                    &nbsp;&nbsp;&nbsp;
                    <asp:DropDownList ID="DropDownList3" runat="server">
                    </asp:DropDownList>
                    &nbsp;<asp:Button ID="Button2" runat="server" style="height: 26px" Text="Ir a Via" />

                    &nbsp;
                    <asp:DropDownList ID="DropDownList4" runat="server">
                    </asp:DropDownList>
                    &nbsp;<asp:Button ID="Button3" runat="server" Text="Ir a Biblioteca" 
                        Width="108px" />

                </ContentTemplate> 

                </asp:UpdatePanel> 
                
                
             </td>
        </tr>
    </table>
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
</p>

<script type = "text/javascript" >
    var map = AspMap.find('<%=map1.ClientID%>');
    map.add_mouseMove(mouseMoveHandler);

    function mouseMoveHandler(sender, e /* MouseEventArgs */) {
        var label = document.getElementById("latlong");
        if (e.isInside)
            label.innerHTML = "Latitude: " + formatCoord(e.latitude, "lat") + " | Longitude: " + formatCoord(e.longitude, "long");
        else
            label.innerHTML = "&nbsp;";
    }
    function formatCoord(coord, type) {
        var dec = Math.abs(coord);
        var deg = Math.floor(dec);
        var min = Math.floor((dec - deg) * 60);
        var sec = Math.floor((dec - deg - (min / 60)) * 3600);
        var suff = (coord > 0 ? "E" : "W"); if (type == "lat") suff = (coord > 0 ? "N" : "S");
        return deg + "&deg; " + min + "' " + sec + '" ' + suff;
    }

    /*
    function mouseMoveHandler(sender, e) {
    var label = document.getElementById("latlong");
    label.innerHTML = "Latitude: " + e.latitude + " | Longitude: " + e.longitude;

    }
    */
    //     function zoomIn() {
    //         map.zoomIn();
    //     }

    //     function zoomOut() {
    //         map.zoomOut();
    //     }

    //     function zoomFull() {
    //         map.zoomFull();
    //     }

    //     function pan(dx, dy) {
    //         map.pan(dx, dy);
    //     }

    function printMap() {
        map.print();
    }

    //     function setTool(tool) {
    //         map.set_mapTool(tool);
    //     }

    function centerAt() {
        var latitude = parseFloat(document.getElementById("lat").value);
        var longitude = parseFloat(document.getElementById("long").value);
        map.centerAt(new AspMap.Point(longitude, latitude));

        //map.CenterAndZoom(new AspMap.Point(longitude, latitude),15) 
    }

    /*
    function initialize() {
    if (GBrowserIsCompatible()) {

    //var map = new GMap2(document.getElementById("map_canvas"));
    map.setCenter(new GLatLng(-18.142854, -63.479326), 16); // el numero 16 indica el zoom de inicio

    //map.setMapType(G_SATELLITE_MAP); // imagen satelital
    //map.setMapType(G_HYBRID_MAP); // mapa Hibrido
    //map.getZoom();
    //map.addOverlay();
    map.addControl(new GMapTypeControl()); // selector de tipo de mapa
    map.addControl(new GLargeMapControl()); // adiciona el zoom al mapa
    map.addControl(new GScaleControl());    // escala del mapa en 
    map.addControl(new GOverviewMapControl()); //  un mini mapa
    map.setMapType(G_SATELLITE_MAP);

    var point = new GLatLng(-18.139938, -63.482909); // darle coordenadas para
    map.addOverlay(new GMarker(point));              // marca el punto

    }
    }
    */

    </script> 
</asp:Content>
