﻿Imports AspMap
Imports AspMap.Web

Imports System.Drawing

Imports System
Imports System.Web.UI
Imports AspMap.Web.Extensions

Imports ServerControlConverter
Imports AspMap.CoordSystem

Imports Microsoft.VisualBasic

Imports System.Collections
Imports System.ComponentModel
Imports System.Data

Imports System.Web
Imports System.Web.SessionState

Imports System.Web.UI.WebControls
Imports System.Web.UI.HtmlControls

Public Class index
    Inherits System.Web.UI.Page

#Region "GOOGLEMAP"
    Private Sub AddGoogleMapsLayer()
        ' You have to sign up for a Maps API key at http://code.google.com/apis/maps/signup.html.
        ' The key below is for http://localhost/ only.

        Dim gml As GoogleMapsLayer = New GoogleMapsLayer("ABQIAAAAYnPRQnqmO-MqUaKXVmkCZBT2yXp_ZAY8_ufC3CFXhHIE1NvwkxQTvzijMeKMNOJsSSC4Nb9CvFNPKg", GoogleMapType.Normal)

        Map1.BackgroundLayer = gml
        Map1.ImageFormat = ImageFormat.Png
        Map1.ImageOpacity = 0.40000000000000002
        ' When a GoogleMapsLayer object is set as a background layer: 1) the coordinate system of the
        ' Map control will be set to PCS_PopularVisualisationMercator; 2) 20 zoom levels
        ' from Google Maps will be added to the ZoomLevels collection of the Map control;
        ' 3) the FullExtent property of the Map control will be set to the full extent of Google Maps.


    End Sub
#End Region

#Region "adicionando mapas"
    Private Sub addMapas()
        Dim layer As AspMap.Layer
        'Dim layer1 As Layer

        Dim MapDir As String = MapPath("shapes/UAGRM/")


        layer = Map1.AddLayer(MapDir & "Modulos.shp")
        layer.Description = "Modulos"

        layer = Map1.AddLayer(MapDir & "Canchas.shp")
        'layer.Visible = True
        layer.Description = "Canchas"

        layer = Map1.AddLayer(MapDir & "Patio_de_Comida.shp")
        layer.Description = "Patio de Comida"


        layer = Map1.AddLayer(MapDir & "VEHICULAR.shp")
        layer.Description = "Via Vehicular"

        layer = Map1.AddLayer(MapDir & "PEATONAL.shp")
        layer.Description = "Via Peatonal"

        layer = Map1.AddLayer(MapDir & "Auditorios.shp")
        layer.Description = "Auditorios"


        layer = Map1.AddLayer(MapDir & "Bibliotecas.shp")
        layer.Description = "Bibliotecas"

        layer = Map1.AddLayer(MapDir & "Fotocopiadoras.shp")
        layer.Description = "Fotocopiadoras"

        layer = Map1.AddLayer(MapDir & "Centros_Internos.shp")
        layer.Description = "Centros Internos"
        'layer = Map1.AddLayer(MapDir & "modulo.png")
        layer = Map1.AddLayer(MapDir & "BAÑOS DE HOMBRES.shp")
        layer.Description = "Baños de Hombre"

        layer = Map1.AddLayer(MapDir & "BAÑOS DE MUJERES.shp")
        layer.Description = "Baños de Mujeres"

        layer = Map1.AddLayer(MapDir & "Facultades.shp")
        layer.Description = "Facultades"

        layer = Map1.AddLayer(MapDir & "Entradas.shp")
        layer.Description = "Entradas"

    End Sub
#End Region

#Region "propiedades de los shapes"
    Private Sub PropiedadesShapes()
        Dim layer As AspMap.Layer
        'Dim layer1 As Layer

        Dim MapDir As String = MapPath("shapes/UAGRM/")

        'propiedades de los Modulos
        layer = Map1("Modulos")
        layer.LabelField = "Nombre"
        layer.Symbol.FillColor = Color.Tomato
        layer.ShowLabels = True
        layer.LabelFont.Name = "verdana"
        layer.LabelFont.Color = Color.Black
        layer.LabelFont.Size = 16
        layer.LabelFont.Bold = True
        layer.LabelStyle = LabelStyle.PolygonCenter
        layer.Symbol.Size = 2

        '--------------------CANCHAS------------------------

        layer = Map1("Canchas")
        layer.LabelField = "Nombre" ' muestra el id
        layer.Symbol.FillColor = Drawing.Color.Green
        layer.ShowLabels = True
        layer.LabelFont.Name = "verdana"
        layer.LabelFont.Size = 12
        layer.LabelFont.Bold = True ' negrilla
        layer.LabelStyle = LabelStyle.PolygonCenter
        layer.Symbol.Size = 1

        'propiedades de los Patio de comida---------------------------

        layer = Map1("Patio_de_Comida")
        layer.LabelField = "Nombre"
        layer.Symbol.FillColor = Color.Pink
        layer.ShowLabels = False
        layer.LabelFont.Name = "verdana"
        layer.LabelFont.Color = Color.Black
        layer.LabelFont.Size = 16
        layer.LabelFont.Bold = True
        layer.LabelStyle = LabelStyle.PolygonCenter
        layer.Symbol.Size = 2


        '-------------PROPIEDADES TIPO LINEAS-------------------------------'

        ' adiciaonando vias Vehiculares
        layer = Map1("VEHICULAR")
        layer.Symbol.LineColor = Drawing.Color.Yellow
        layer.ShowLabels = False
        layer.LabelFont.Size = 2
        layer.Symbol.Size = 4


        '  via peatonal
        layer = Map1("PEATONAL")
        layer.Symbol.LineColor = Drawing.Color.Brown
        layer.ShowLabels = False
        layer.LabelFont.Size = 2
        layer.Symbol.Size = 1


        '' ADICIONANDO PUNTOS
        ''  Auditorios-------------------------
        layer = Map1("Auditorios")
        layer.Symbol.PointStyle = PointStyle.Bitmap
        layer.Symbol.Bitmap = MapPath("Simbolos/auditorio icono.png")
        layer.Symbol.TransparentColor = Color.White
        layer.Symbol.Size = 17
        'layer.Symbol.FillColor = Drawing.Color.Red
        layer.LabelField = "Nombre"
        layer.ShowLabels = False
        layer.LabelFont.Name = "arial"
        layer.LabelFont.Bold = True
        layer.LabelFont.Size = 14
        layer.LabelFont.Outline = True
        layer.LabelFont.OutlineColor = Drawing.Color.Yellow

        '---------------------------------------------'
        layer = Map1("Bibliotecas")
        layer.Symbol.PointStyle = PointStyle.Bitmap
        layer.Symbol.Bitmap = MapPath("Simbolos/biblio icono.png")
        layer.Symbol.TransparentColor = Color.White
        layer.Symbol.Size = 17
        'layer.Symbol.FillColor = Drawing.Color.Red

        '---------------------------------------------'
        layer = Map1("Fotocopiadoras")
        layer.Symbol.PointStyle = PointStyle.Bitmap
        layer.Symbol.Bitmap = MapPath("Simbolos/church.bmp")
        layer.Symbol.TransparentColor = Color.White
        layer.Symbol.Size = 10

        layer = Map1("Centros_Internos")
        layer.Symbol.PointStyle = PointStyle.Bitmap
        layer.Symbol.Bitmap = MapPath("Simbolos/centro interno icono.png")
        layer.Symbol.TransparentColor = Color.White
        layer.Symbol.Size = 10

        layer = Map1("BAÑOS DE HOMBRES")
        layer.Symbol.PointStyle = PointStyle.Bitmap
        layer.Symbol.Bitmap = MapPath("Simbolos/baño hombre icono.png")
        layer.Symbol.TransparentColor = Color.White
        layer.Symbol.Size = 10

        layer = Map1("BAÑOS DE MUJERES")
        layer.Symbol.PointStyle = PointStyle.Bitmap
        layer.Symbol.Bitmap = MapPath("Simbolos/baño mujer icono.png")
        layer.Symbol.TransparentColor = Color.White
        layer.Symbol.Size = 10

        layer = Map1("Facultades")
        layer.Symbol.PointStyle = PointStyle.Bitmap
        layer.Symbol.Bitmap = MapPath("Simbolos/facultad icono.png")
        layer.Symbol.TransparentColor = Color.White
        layer.Symbol.Size = 10

        layer = Map1("Entradas")
        layer.Symbol.PointStyle = PointStyle.Bitmap
        layer.Symbol.Bitmap = MapPath("Simbolos/entrada icono.png")
        layer.Symbol.TransparentColor = Color.White
        layer.Symbol.Size = 10


    End Sub
#End Region

#Region "Zoom"
    Public Sub zoom()
        'Map1.ZoomLevels.Add(23400000, "Modulos")
        'Map1.ZoomLevels.Add(12000000)
        'Map1.ZoomLevels.Add(10250, "AreasVerdes") '510mts
        Map1.ZoomLevels.Add(7125, "Modulos")  '360 mts
        Map1.ZoomLevels.Add(3562, "Areas Verdes")  '110 mts
        Map1.ZoomLevels.Add(2000, "Vias")  '30 mts
    End Sub
#End Region

#Region "Scala"
    Public Sub Scala()
        Map1.MapUnit = AspMap.MeasureUnit.Kilometer  ' kilometro
        Map1.ScaleBar.Visible = True
        Map1.ScaleBar.Position = ScaleBarPosition.TopRight
        Map1.ScaleBar.BarUnit = UnitSystem.Metric
    End Sub
#End Region

#Region "Mostrando Lista de Capas"
    Private Sub MostrarListaCapas()
        If IsPostBack Then
            Return
        End If

        For Each layer As AspMap.Layer In Map1
            Dim item As ListItem = New ListItem(layer.Description, layer.Name)
            item.Selected = layer.Visible
            layerlist.Items.Add(item)
        Next layer
    End Sub

    Public Sub actualizarVistaCapas()
        If (Not IsPostBack) Then
            Return
        End If

        For Each item As ListItem In layerlist.Items
            Map1(item.Value).Visible = item.Selected
        Next item
    End Sub
#End Region

#Region "cargar datos para seleccionar"
    Public Sub CargarModulos()
        If (Not IsPostBack) Then
            DropDownList2.DataSource = Map1("Modulos").Recordset
            DropDownList2.DataTextField = "Modulo"
            DropDownList2.DataValueField = "Modulo"
            DropDownList2.DataBind()
        End If
    End Sub

    Public Sub CargarVias()
        If (Not IsPostBack) Then
            DropDownList3.DataSource = Map1("VEHICULAR").Recordset
            DropDownList3.DataTextField = "Nombre"
            DropDownList3.DataValueField = "Nombre"
            DropDownList3.DataBind()
        End If
    End Sub

    Public Sub CargarBibliotecas()
        If (Not IsPostBack) Then
            DropDownList4.DataSource = Map1("Bibliotecas").Recordset
            DropDownList4.DataTextField = "Nombre"
            DropDownList4.DataValueField = "Nombre"
            DropDownList4.DataBind()
        End If
    End Sub

#End Region


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        ' falta que cuando seleccione en el combo box cambie lo que quiera seleccionar sean modulos, vias, ect

        Map1.CoordinateSystem = New AspMap.CoordSystem(AspMap.CoordSystemCode.GCS_WGS84)

        Scala()
        zoom()


        If CheckBox1.Checked = True Then
            AddGoogleMapsLayer()
        End If

        addMapas()

        Map1.ImageFormat = AspMap.ImageFormat.Png
        'Map1.ImageOpacity = 0.7



        If (Not IsPostBack) Then
            Map1.CenterAt(Map1.CoordinateSystem.FromWgs84(-63.195999999999998, -17.776))
            Map1.ZoomLevel = 15
        End If

        PropiedadesShapes()
        MostrarListaCapas()
        CargarModulos()
        CargarVias()
        CargarBibliotecas()
    End Sub

    Private Sub index_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        actualizarVistaCapas()
        AddModuloTooltips()
    End Sub


#Region "info windows"

    Protected Function GetCalloutText(ByVal rs As AspMap.Recordset) As String
        Dim index As Integer = rs.Fields.GetFieldIndex("Nombre")
        If index < 0 Then
            index = rs.Fields.GetFieldIndex(rs.Layer.LabelField)
        End If
        If index < 0 Then
            index = 0
        End If
        Return rs(index).ToString()
    End Function

    Protected Sub Map1_InfoWindowTool(ByVal sender As Object, ByVal e As AspMap.Web.InfoWindowToolEventArgs) Handles Map1.InfoWindowTool
        Dim records As AspMap.Recordset = Map1.Identify(e.InfoPoint, 5)

        e.InfoWindow.Width = 300
        e.InfoWindow.HorizontalAlign = HorizontalAlign.Center
        e.InfoWindow.Font.Bold = True
        e.InfoWindow.ScrollBars = System.Web.UI.WebControls.ScrollBars.Auto

        ' set the content of the info window
        e.InfoWindow.Content = GetCalloutText(records)

        ' add a DataGrid control to the info window to render the records
        If (Not records.EOF) Then
            Dim grid As DataGrid = New DataGrid()
            grid.DataSource = records
            grid.DataBind()
            'grid.BorderColor = Color.AliceBlue
            e.InfoWindow.Controls.Add(grid)
        End If
    End Sub


    Private Sub AddModuloTooltips()
        Map1.TooltipStyle.BackColor = Color.LightYellow
        Map1.TooltipStyle.Width = System.Web.UI.WebControls.Unit.Pixel(220)

        Dim records As AspMap.Recordset = Map1("Modulos").SearchShape(Map1.Extent, AspMap.SearchMethod.Inside)

        'If Not records.EOF Then

        ''Map1.Extent = records.Extent

        Do While Not records.EOF
            Dim tooltip As String = "<b> " & records("Modulo") & "</b>"

            'Map1.MapShapes.Clear()

            'arreglar
            ' Dim shape As MapShape = Map1.MapShapes.Add(records.Shape)
            'Dim s As Symbol = New Symbol()
            ' s.FillStyle = AspMap.FillStyle.Invisible
            '  s.LineColor = Color.Aqua
            '   s.Size = 3
            '  shape.Symbol = s
            '    Map1.MapShapes.Add(records.Shape)



            Map1.Hotspots.AddInfo(records.RecordExtent.Center, 9, tooltip, records("ID").ToString())
            records.MoveNext()
        Loop
        '' End If

    End Sub
#End Region

    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As EventArgs) Handles Button1.Click

        Dim state As Recordset
        state = Map1("Modulos").SearchExpression("Modulo=""" & DropDownList2.SelectedValue & """")
        If Not state.EOF Then

            Map1.Extent = state.Extent


            ' select the state shape with a red outline
            Map1.MapShapes.Clear()
            Dim shape As MapShape = Map1.MapShapes.Add(state.Shape)
            Dim s As Symbol = New Symbol()
            s.FillStyle = AspMap.FillStyle.Solid
            s.LineColor = Color.Aqua
            s.Size = 3
            shape.Symbol = s
        End If
    End Sub

    Protected Sub Button2_Click(ByVal sender As Object, ByVal e As EventArgs) Handles Button2.Click
        'seleccion de estado  search for a state
        Dim state As Recordset
        state = Map1("VEHICULAR").SearchExpression("Nombre=""" & DropDownList3.SelectedValue & """")

        If Not state.EOF Then
            Map1.Extent = state.RecordExtent

            ' select the state shape with a red outline
            Map1.MapShapes.Clear()
            Dim shape As MapShape = Map1.MapShapes.Add(state.Shape)
            Dim s As Symbol = New Symbol()
            s.FillStyle = AspMap.FillStyle.Invisible
            s.LineColor = Color.Aqua
            s.Size = 3
            shape.Symbol = s
        End If
    End Sub

    Protected Sub Button3_Click(ByVal sender As Object, ByVal e As EventArgs) Handles Button3.Click
        'seleccion de estado  search for a state
        Dim state As Recordset
        state = Map1("Bibliotecas").SearchExpression("Nombre=""" & DropDownList4.SelectedValue & """")

        If Not state.EOF Then
            Map1.Extent = state.RecordExtent
            Map1.ZoomLevel = 7

            ' select the state shape with a red outline
            Map1.MapShapes.Clear()
            Dim shape As MapShape = Map1.MapShapes.Add(state.Shape)
            Dim s As Symbol = New Symbol()
            s.FillStyle = AspMap.FillStyle.Invisible
            s.LineColor = Color.Aqua
            s.Size = 3
            shape.Symbol = s


        End If
    End Sub
End Class